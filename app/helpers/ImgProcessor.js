const sharp = require('sharp')

module.exports = {

    async thumbIt(path, dest) {
        return sharp(path)
            .resize(200, 200)
            .toFile(dest)
    }

}