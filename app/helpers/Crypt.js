const crypto = require('crypto')

module.exports = {

    random(size) {
        return crypto.randomBytes(size/2).toString('hex')
    }

}