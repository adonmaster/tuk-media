const cron = require('node-cron')
const moment = require('moment')
const { Media } = require('../models')
const { Op } = require('sequelize')
const s4 = require('../helpers/s4')

module.exports = {

    init() {
        vlog.log('scheduleCleanOwned registered')
        cron.schedule('0 */3 * * *', this.scheduleCleanOwned, {})
    },

    async scheduleCleanOwned()
    {
        vlog.log('initializing scheduleCleanOwned')

        let fewHoursAgo = moment().subtract(3, 'hours').toDate()
        try {

            const medias = await Media.findAll({
                attributes: ['id', 'disk', 'path_full', 'path_thumb'],
                limit: 200,
                where: { owner: null, file_deleted_at: null, created_at: {[Op.lt]: fewHoursAgo} }
            })

            //
            for (const media of medias) {

                if (media.disk.startsWith('aws')) {

                    const bucket = media.disk.split('aws:s3:')[1]
                    const files = [media.path_full]
                    if (media.path_thumb) files.push(media.path_thumb)
                    vlog.log(`aws about to delete ${files.join(', ')}`)

                    await s4.deleteFileFrom(bucket, files)

                    media.file_deleted_at = moment()
                    await media.save()
                }

            }

            vlog.log('terminating scheduleCleanOwned')

        } catch(e) {
            vlog.error(e.message, e)
        }
    }
}