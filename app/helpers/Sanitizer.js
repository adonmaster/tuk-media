const Str = require("./Str");

const _signatureDef = 'Sanitizer-uid421-default-value'

const Sanitizer = {

    _isObjSignedDef(obj) {
        return (obj instanceof Object) && obj['signature'] === _signatureDef
    },

    rules: {
        def: v => ({signature: _signatureDef, def: v}),
        uppercase: v => String(v).toUpperCase(),
        digitsOnly: Str.digitsOnly,
        dropFirst: v => String(v).substring(1),
        trim: s => String(s).trim()
    },

    /**
     * @typedef {string[]} mongo
     * @param {Object} attrs
     * @param {Object<string,string[]>} rules
     *
     * Ex.: run({a: 'ab': dois: 2}, {a: ['uppercase', 'dropfirst']}) => {a: 'B', dois: 2}
     */
    run(attrs, rules)
    {
        let r = Object.assign({}, attrs)

        Object.keys(rules).forEach(key => {
            let v = attrs[key]

            let ruleList = rules[key]
            if ( ! Array.isArray(ruleList)) ruleList = [ruleList]

            ruleList.forEach((rule) => {
                if (this._isObjSignedDef(rule)) {
                    v = v || rule['def']
                } else if (rule instanceof Function) {
                    v = rule(v)
                } else {
                    throw new Error('Use apenas funções ou def')
                }
            })

            r[key] = v
        })

        return r
    }

}

/**
 *
 * @type {{Sanitizer: {}, uppercase: Function, digitsOnly: Function, dropFirst: Function,
 * trim: Function, def: Function(defaultValue):Object}}
 */
module.exports = {
    Sanitizer, ...Sanitizer.rules
}