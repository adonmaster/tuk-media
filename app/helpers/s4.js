const path = require('path')
const fs = require('fs')
const aws = require('aws-sdk')

module.exports = {

    uploadFileAndRetrieveUrl(path_full)
    {
        const filename = path.basename(path_full);

        return new Promise((resolve, reject) => {
            fs.readFile(path_full, (err, data) => {
                if (err) return reject(err)

                const s3Instance = new aws.S3({params: { Bucket: process.env.AWS_S3_BUCKET_NAME }})
                const object = {Key: filename, Body: data, ContentType: 'AUTO_CONTENT_TYPE'}

                s3Instance.putObject(object)
                    .promise()
                    .then(()=>{
                        let url = `${process.env.AWS_CDN_BASE_URL}/${encodeURIComponent(filename)}`
                        resolve(url)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        })
    },

    /**
     *
     * @param {string} bucket
     * @param {array} paths
     * @returns {Promise<void>}
     */
    async deleteFileFrom(bucket, paths) {
        if (bucket) {
            try {
                const s3Instance = new aws.S3({params: {Bucket: bucket}})

                const payload = {Delete: { Objects: paths.map(s => ({Key: s})) }}

                const r = await s3Instance.deleteObjects(payload).promise()
                vlog.log(JSON.stringify(r))

            } catch(e) {
                vlog.error(e)
            }
        }
    }

}
