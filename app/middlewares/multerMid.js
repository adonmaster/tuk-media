const multer = require('multer')
const Paths = require('../../Paths')
const crypto = require('crypto')

function _filenameGenerator(req, file, cb) {
    crypto.randomBytes(16, (err, buf) => {
        if (err) return cb(err)
        const filename = `${buf.toString('hex')}-${file.originalname}`

        file.key = filename

        cb(null, filename)
    })
}

module.exports = {

    single(fieldName) {

        const upload = multer({
            dest: Paths.cacheDir(),
            storage: multer.diskStorage({
                destination: (req, file, cb) => { cb(null, Paths.cacheDir()) },
                filename: _filenameGenerator
            }),
            limits: {
                fileSize: 10 * 1024 * 1024
            },
            fileFilter: (req, file, cb) => {
                if (['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif', 'image/wepp'].includes(file.mimetype)) {
                    file.db_type = 'image'
                    cb(null, true)
                }
                else if (['application/pdf'].includes(file.mimetype)) {
                    file.db_type = 'pdf'
                    cb(null, true)
                }
                else {
                    cb(new Error('Unsupported mime type.'))
                }
            }
        })

        return upload.single(fieldName)
    }

}