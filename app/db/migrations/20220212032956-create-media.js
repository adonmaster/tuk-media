'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('medias', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      uid: { type: Sequelize.STRING, required: true, unique: true },
      type: { type: Sequelize.STRING, required: true },
      disk: { type: Sequelize.STRING, required: true },

      path_full: { type: Sequelize.STRING, required: true },
      path_thumb: { type: Sequelize.STRING },

      url_full: { type: Sequelize.STRING, required: true },
      url_thumb: { type: Sequelize.STRING },

      owner: { type: Sequelize.STRING },

      file_deleted_at: {allowNull: true, type: Sequelize.DATE},
      created_at: { allowNull: false, type: Sequelize.DATE },
      updated_at: { allowNull: false, type: Sequelize.DATE }
    }).then(() => {
      queryInterface.addIndex('medias', ['uid'], {token: {name: 'medias__uid', unique: true}})
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('medias');
  }
};