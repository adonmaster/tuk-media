'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  class Media extends Model {
    static associate(models) {
      // define association here
    }
  }

  Media.init({

    uid: DataTypes.STRING,
    type: DataTypes.STRING,
    disk: DataTypes.STRING,

    path_full: DataTypes.STRING,
    path_thumb: DataTypes.STRING,

    url_full: DataTypes.STRING,
    url_thumb: DataTypes.STRING,

    owner: DataTypes.STRING,

    file_deleted_at: DataTypes.DATE

  }, {
    sequelize,
    modelName: 'Media', tableName: 'medias', createdAt: 'created_at', updatedAt: 'updated_at'
  });

  return Media;
};