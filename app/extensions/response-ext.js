const Validator = require('validatorjs')
const { Sanitizer } = require('../helpers/Sanitizer')

module.exports = app => {

    app.response.j = function (message, status = 200, payload = {}) {
        return this.status(status).json({message, payload})
    }

    app.response.jok = function (message = 'Ok', status = 200, payload = {}) {
        return this.j(message, status, payload)
    }

    app.response.jerr = function (message, status = 400, payload = {}) {
        return this.j(message, status, payload)
    }

    app.response.jpay = function (payload, message = 'Ok!', status = 200) {
        return this.j(message, status, payload)
    }

    app.response.validate = function (rules={})
    {
        const body = this.req.body
        const validator = new Validator(this.req.body, rules)

        if (validator.fails()) {
            this.j('Erro de validação', 422, validator.errors)
            return false
        }

        return body
    }

    app.response.validateAndSanitize = function(rules, sanitizeRules)
    {
        const body = this.validate(rules)
        if (body === false) return false

        return Sanitizer.run(body, sanitizeRules)
    }
}