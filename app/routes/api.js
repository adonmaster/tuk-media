module.exports = router = require('express').Router()

const multer = require('multer')
const multerMid = require('../middlewares/multerMid')

const MediaController = require('../controllers/MediaController')

//
router.post('/', multerMid.single('file'), MediaController.store)
router.post('/owner', MediaController.owner)