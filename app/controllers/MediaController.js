const path = require('path')
const Crypt = require('../helpers/Crypt')
const Paths = require("../../Paths");
const ImgProcessor = require('../helpers/ImgProcessor')
const s4 = require('../helpers/s4')
const fs = require('fs')
const { def } = require('../helpers/Sanitizer')

const { Media } = require('../models')

module.exports = {

    async store(req, res, next) {
        try {

            const f = req.file

            // unique uid
            const uid = Crypt.random(30)

            // thumb
            const isImage = f.db_type === 'image'
            const pathFull = isImage ? path.resolve(f.destination, f.filename) : null
            const filenameThumb = isImage ? 'thumb_' + f.key : null
            const pathThumb = isImage ? Paths.cacheDir(filenameThumb) : null
            if (isImage) await ImgProcessor.thumbIt(pathFull, pathThumb)

            //
            const url_full = await s4.uploadFileAndRetrieveUrl(pathFull)
            const url_thumb = isImage ? await s4.uploadFileAndRetrieveUrl(pathThumb) : null

            //
            const model = await Media.create({
                uid,
                type: f.db_type,
                disk: 'aws:s3:' + process.env.AWS_S3_BUCKET_NAME,
                path_full: f.filename, path_thumb: filenameThumb,
                url_full, url_thumb
            })
            await model.save();

            // cleanup tmp files
            fs.unlink(pathFull, err => {})
            if (pathThumb) fs.unlink(pathThumb, err => {})

            return res.jpay(model)

        } catch(err) {
            return res.jerr(err.message)
        }
    },

    async owner(req, res, next) {

        const data = res.validateAndSanitize({
            'uids': 'required|array'
        }, {
            'owner': def(null)
        })
        if (data === false) return

        const { uids, owner } = data

        try {

            const affected = await Media.update({ owner }, { where: {uid: uids} })

            return res.jok(`${affected} affected`)

        } catch (err) {
            return res.jerr(err.message)
        }

    }

}
