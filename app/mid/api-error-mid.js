const apiErrorMid = async (err, req, res, next) => {
    if (err) return res.jerr(err.message, 500, err)
    next()
}

module.exports = apiErrorMid