const path = require('path')

module.exports = {

    storageDir(append='') {
        return path.resolve(__dirname, 'public', 'storage', append)
    },

    cacheDir(append='') {
        return path.resolve(__dirname, 'storage', 'cache', append)
    }

}