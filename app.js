global.vlog = require('./app/helpers/VirtualLog')

//
require('dotenv').config()
const express = require('express')
const app = express()

// encoding
app.use(require('cors')())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// utils
const morgan = require('morgan')
app.use(morgan('tiny'))

// response
const responseValidationExt = require('./app/extensions/response-ext')
responseValidationExt(app)

// -- Routes --
app.use('/api', require('./app/routes/api'), require('./app/mid/api-error-mid'))
app.use('/', (_, res) => {
    let html = `<b>tuk-media</b> v${process.env.APP_VERSION} - ${(new Date).toLocaleString()}<br><br>`
    html = html + vlog.render()
    res.send(html)
})

// cron jobs
const Cron = require('./app/helpers/Cron')
Cron.init()


//
app.once('SIGUSR2', async () => {
    process.kill(process.pid, 'SIGUSR2')
})

// disconnect on exit
process.on('SIGINT', async () => {
    process.exit(0)
})

// listening ...
const port = 4002
app.listen(port, () => {
    vlog.fix('listen', `Running on ${process.env.APP_URL}:${port}`)
})